import { Injectable, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class CountryService {

  constructor(private http: HttpClient) {}

// getCountry() {
//   return this.http.get('https://restcountries.eu/rest/v2/lang/es')
//              .pipe( map( ( countries: any[] ) => {
//                return countries.map( country => {
//                  return {
//                    name  : country.name,
//                    codigo: country.alpha3Code

//                  };
//                });
//              }));
// }

// Codigo SIMILAR

getCountry() {
  return this.http.get('https://restcountries.eu/rest/v2/lang/es')
             .pipe( map( ( countries: any[] ) =>  countries.map( country => ( { name: country.name, codigo: country.alpha3Code } ) )  ) );
}


}
