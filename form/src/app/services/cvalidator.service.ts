import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { from, Observable } from 'rxjs';


interface ErrorValidate{
  // conjunto de llaves devolvera un valor booleano
  [s: string]: boolean;
}

@Injectable({
  providedIn: 'root'
})

export class CvalidatorService {

  constructor() { }



  existeUsuario( control: FormControl ): Promise<ErrorValidate> | Observable<ErrorValidate> {
    if( !control.value ){
      return Promise.resolve( null );
    }

    return new Promise( (resolve, reject) => {
      setTimeout( () => {
        if( control.value === 'stride' ) {
          resolve( {existe: true } );
        } else {
          resolve( null );
        }
      }, 3600 );
    });

   }

  noHerrera( control: FormControl ): ErrorValidate  {
    if ( control.value?.toLowerCase() === 'herrera') {
      return {
        noHerrera: true
      }
    }
    return null;

  }

  passwordsIguales( pass1Name: string, pass2Name: string ) {
    // fromGruoup -- porque las validaciones estamos haciendo dentro de un formulario
    return ( formGroup: FormGroup ) => {
      const pass1Control = formGroup.controls[pass1Name];
      const pass2Control = formGroup.controls[pass2Name];

      if ( pass1Control.value === pass2Control.value ) {
        // Paso la validacion
        pass2Control.setErrors(null);
      } else {
        pass2Control.setErrors({noEsigual: true});
      }
    }
  }
}
