import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { CvalidatorService } from '../../services/cvalidator.service';

@Component({
  selector: 'app-reactive',
  templateUrl: './reactive.component.html',
  styleUrls: ['./reactive.component.css']
})
export class ReactiveComponent implements OnInit {

  form: FormGroup;

  // EL Constructor se crea primero antes que el Template
  constructor(private formBuilder: FormBuilder,
              private customValidator: CvalidatorService ) {
    this.createForm();

    this.loadFormData();

    this.createListener();
  }

  ngOnInit(): void {
  }

  get pasatiempos() {
    return this.form.get('pasatiempos') as FormArray;
  }

  get isInvalidName(): boolean {
    return this.form.get('name').invalid && this.form.get('name').touched;
  }

  get isInvalidLastName() {
    return this.form.get('lastname').invalid && this.form.get('lastname').touched;
  }

  get isInvalidEmail() {
    return this.form.get('email').invalid && this.form.get('email').touched;
  }

  get isInvalidUsuario() {
    return this.form.get('user').invalid && this.form.get('user').touched;
  }

  get isInvalidDistrito() {
    return this.form.get('direccion.distrito').invalid && this.form.get('direccion.distrito').touched;
  }

  get isInvalidCiudad() {
    return this.form.get('direccion.ciudad').invalid && this.form.get('direccion.ciudad').touched;
  }

  get isInvalidPass1() {
    return this.form.get('pass1').invalid && this.form.get('pass1').touched;
  }

  get isInvalidPass2() {

    const pass1 = this.form.get('pass1').value;
    const pass2 = this.form.get('pass2').value;

    return ( pass1 === pass2 ) ? false : true;

    // return this.form.get('pass1').invalid && this.form.get('pass1').touched;
  }


  

  // setValue --> tiene que estar especificarse los campos de mi formulario
  // rest     --> No necesariamente tiene espcificarse todos los campos.`

  createForm() {
    this.form = this.formBuilder.group ( {

      // Primer parametros es valor por defecto
      // Segundo parametro es validacion sincrona
      // Tercer parametro es validacion asintrona
      name      : [ '', [ Validators.required, Validators.minLength( 5 ) ]],
      //  Ya no es necesario necesario anexar el parametro  en el metodo.
      lastname  : [ '', [ Validators.required, this.customValidator.noHerrera ]],
      email     : [ '', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$') ] ],
      user      : [ '', , this.customValidator.existeUsuario ],
      pass1     : [ '', Validators.required ],
      pass2     : [ '', Validators.required ],
      // [ara crear una objeto sobre otro]
      direccion : this.formBuilder.group( {
        distrito:  [ '', [Validators.required, Validators.minLength( 5 )] ],
        ciudad  :  [ '', [Validators.required, Validators.minLength( 5 )] ]
      }),
      // Creando un arreglo
      pasatiempos: this.formBuilder.array( [] )
    },{
      validators: this.customValidator.passwordsIguales('pass1', 'pass2')
    }

    );
  }

  createListener() {
    // this.form.valueChanges.subscribe( valor => {
    //   console.log( valor );
    // });

    // this.form.statusChanges.subscribe( valor => { console.log( valor )});

    this.form.get('name').valueChanges.subscribe( console.log );
  }


  loadFormData(){
    // this.form.setValue({
      this.form.reset({
      name      : 'Elvis',
      lastname  : 'Huacachi',
      email     : 'ehuacachi@gmail.com',
      pass1     : '123456',
      pass2     : '123456',
      direccion : {
        distrito:  'Villa el Salvador',
        ciudad  :  'Limaaaa'
      }
    });
  }
  

  save() {
    // No necesito recibir los parametros del temple xq ya se ha construido form
    
    console.log( this.form );

    if ( this.form.invalid ) {
      return Object.values (this.form.controls).forEach( control => {
        if ( control instanceof FormGroup ){
          Object.values (control.controls).forEach( control => control.markAsTouched())

        } else {
          control.markAsTouched();
        }
      });
    }

    console.log ( this.form.value );

    // Despues del posteo de la informacion
    this.form.reset({
      //Para variables que son listas.. colocar cualquier valor.
      nombre: 'Sin nombre'
    });
  }


  agregarPasatiempo() {
    this.pasatiempos.push( this.formBuilder.control('') );
  }

  borrarPasatiempo(i: number) {
    this.pasatiempos.removeAt(i);
  }

}
