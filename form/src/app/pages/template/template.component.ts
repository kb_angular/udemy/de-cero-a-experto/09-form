import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';


import { CountryService } from '../../services/country.service';

@Component({
  selector: 'app-template',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.css']
})
export class TemplateComponent implements OnInit {

  user = {
    name: 'Elvis',
    email: 'ehuacachi@gmail.com',
    lastname: 'Huacachi',
    country: 'PER',
    sex: ''
  };

  countries: any[] = [];

  constructor(private countryService: CountryService) { }

  ngOnInit(): void {
    this.countryService.getCountry()
        .subscribe( countries => {
          this.countries = countries;

          // Agregar un nuevo elemento
          this.countries.unshift({
            name: 'Seleccione',
            codigo: ''
          });

          console.log( countries );
        });
  }

  guardar( f: NgForm ) {
    // console.log( f );
    
    // invalid --> es del todos los componentes
    if ( f.invalid ){
      Object.values( f.controls ).forEach ( control => {
        // Lo marcamos que ha sido tocado o manipulado para visualizar el mensaje de error.
        control.markAsTouched();
      });
      return;
    }
    
    console.log( f.value );
  }

}
